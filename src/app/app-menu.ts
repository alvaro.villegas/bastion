import { MenuItems } from '../fw/services/menu.service';

export let initialMenuItems: Array<MenuItems> =[
  {
    text: 'Dashboard',
    icon: 'glyphicon-dashboard',
    route: '/dashboard',
    submenu: null,
  },
  {
    text: 'Countries',
    icon: 'glyphicon-flag',
    route: '/countries',
    submenu: null,
  },
  {
    text: 'Settings',
    icon: 'glyphicon-wrench',
    route: '/settings',
    submenu: null,
  },
]