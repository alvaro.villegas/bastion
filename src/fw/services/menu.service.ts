import { Injectable } from '@angular/core';

export interface MenuItems{
  text: string,
  icon: string,
  route: string,
  submenu: Array<MenuItems>
}

@Injectable()
export class MenuService{
  items: Array<MenuItems>;
  isVertical = false;
  showingLeftSideMenu = false;
  
  toggleLeftSideMenu():void{
    this.isVertical=true
    this.showingLeftSideMenu = !this.showingLeftSideMenu;
  }
  
}

