import {Directive, OnDestroy, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {ScreenService} from '../services/screen.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({selector: '[screenLarge]'})
export class ScreenLarge implements OnDestroy{
  private hasView: boolean;
  private screenSubscription: Subscription;
  
  constructor(private viewContainer: ViewContainerRef,
              private template: TemplateRef<Object>,
              private screenService: ScreenService) {
                
    this.screenService.recize$.subscribe(()=>this.onResize());
  }
  @Input()
  set screenLarge(condition){
    condition = this.screenService.screenWidth >= this.screenService.largeBreakpoint;
  
    if(condition && !this.hasView){
      this.hasView=true;
      this.viewContainer.createEmbeddedView(this.template);
    } else if(!condition && this.hasView){
      this.hasView=false;
      this.viewContainer.clear();
    }
}
  ngOnDestroy(){
    this.screenSubscription.unsubscribe();
  }
  onResize(){
    this.screenLarge=false;
  }
  
}