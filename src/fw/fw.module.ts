import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FrameworkBodyComponent } from './framework-body/framework-body.component';
import {ContentComponent} from './content/content.component';
import {MenuComponent} from './menu/menu.component';
import { FrameworkConfigService } from './services/framework-config.service';
import {TopBarComponent} from './top-bar/top-bar.component';
import {StatusBarComponent} from './status-bar/status-bar.component';
import {ScreenService} from './services/screen.service';
import {ScreenLarge} from './directives/screen-large.directives';
import {ScreenBelowLarge} from './directives/screen-below-large.diretives';
import {MenuService} from './services/menu.service';
import {MenuEComponent} from './menus-bar/menu-e/menu-e.component';
import {MenuItemComponent}from './menus-bar/menu-item/menu-item.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    FrameworkBodyComponent,
    ContentComponent,
    MenuComponent,
    TopBarComponent,
    StatusBarComponent,
    ScreenLarge,
    ScreenBelowLarge,
    MenuEComponent,
    MenuItemComponent
  ],
  exports: [
    FrameworkBodyComponent
  ],
  providers:[
    FrameworkConfigService,
    ScreenService,
    MenuService 
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class FwModule { }
