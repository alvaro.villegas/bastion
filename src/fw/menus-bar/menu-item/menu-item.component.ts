import { Component,Input, OnInit } from '@angular/core';
import { MenuItems, MenuService } from 'src/fw/services/menu.service';

@Component({
  selector: 'fw-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {
@Input() item=<MenuItems>null; //:MenuItems;

  constructor(public menuService:MenuService) { this.item}

  ngOnInit(): void {
  }

}
