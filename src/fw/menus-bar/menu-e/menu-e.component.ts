import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/fw/services/menu.service';

@Component({
  selector: 'fw-menu-e',
  templateUrl: './menu-e.component.html',
  styleUrls: ['./menu-e.component.scss']
})
export class MenuEComponent implements OnInit {

  constructor(public menuService: MenuService) { }

  ngOnInit(): void {
  }

}
