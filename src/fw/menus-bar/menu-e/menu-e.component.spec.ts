import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuEComponent } from './menu-e.component';

describe('MenuEComponent', () => {
  let component: MenuEComponent;
  let fixture: ComponentFixture<MenuEComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuEComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuEComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
